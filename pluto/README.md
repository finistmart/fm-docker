
1. place app wars to 

pluto/tomcat/webapps


2. place pluto distribution to pluto/

pluto/pluto-3.1.1-SNAPSHOT-bundle.tar.bz2
 
3. put jandex jar to pluto/tomcat

pluto/tomcat/lib/jandex-1.2.2.Final.jar

4. build image

docker build .

6. get instance id 

docker ps -a

5. run instance 

sudo docker run -d ${IMAGE\_ID}

6. connect 

sudo docker -it ${INSTANCE\_ID} /bin/bash

check for ip

hostname -i

and/or open in browser

http://172.17.0.2:8080/
