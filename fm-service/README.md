
1. place fm-service.war to 

pluto/tomcat/webapps

2. place pluto distribution to pluto/

pluto/pluto-3.1.1-SNAPSHOT-bundle.tar.bz2
 
3. build image

docker build .

4. get instance id 

docker ps -a

5. run instance 

sudo docker run -d ${IMAGE\_ID}

6. connect 

sudo docker -it ${INSTANCE\_ID} /bin/bash

check for ip

hostname -i

and/or open test url in browser or check with curl

curl http://172.17.0.x:8080/fm-service/api/categories
